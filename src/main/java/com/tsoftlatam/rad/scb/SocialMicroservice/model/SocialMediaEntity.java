package com.tsoftlatam.rad.scb.SocialMicroservice.model;

import javax.persistence.*;

@Entity(name = "socialmedia")
@SequenceGenerator(name = "socialmedia_id_seq",initialValue = 1, sequenceName = "socialmedia_id_seq", allocationSize = 1)
public class SocialMediaEntity {

    @Id
    @Column(name = "idsocialmedia")
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "socialmedia_id_seq")
    private Integer idSocialMedia;

    @Column(name = "socialtype")
    private String socialType;

    public SocialMediaEntity() {



    }

    public Integer getIdSocialMedia() {

        return idSocialMedia;

    }

    public void setIdSocialMedia(Integer idSocialMedia) {

        this.idSocialMedia = idSocialMedia;

    }

    public String getSocialType() {

        return socialType;

    }

    public void setSocialType(String socialType) {

        this.socialType = socialType;

    }

}
