package com.tsoftlatam.rad.scb.SocialMicroservice.service.facebook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.Page;
import com.tsoftlatam.rad.scb.SocialMicroservice.configuration.Facebook;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive.CredentialsDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.Credentials;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.InformationLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class FacebookCredentialsImpl implements Credentials {

    @Autowired
    private InformationLoaderService informationLoaderService;
    
    public ResponseDto createCredentials (String pJson, JsonNode pRootNodeCredentials) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        CredentialsDto credentialsDto = mapper.readValue(pJson, CredentialsDto.class);

        String socialType = credentialsDto.getSocialType();

        String shortLivePageAccessToken = credentialsDto.getShortLivePageAccessToken();

        Facebook.idApp = credentialsDto.getIdApp();

        Facebook.appSecret = credentialsDto.getAppSecret();

        JsonNode rootNode = null;

        try {

            Unirest.setTimeouts(0, 0);

            HttpResponse<String> credentials = Unirest.get("https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id="+Facebook.idApp+"&client_secret="+Facebook.appSecret+"&fb_exchange_token="+shortLivePageAccessToken)
                    .asString();

            String json = credentials.getBody();

            ObjectMapper objectMapper = new ObjectMapper();

            rootNode = objectMapper.readTree(json);

        }catch (Exception ex) {

            System.out.println("La excepcion fue: "+ex);

        }

        Facebook.pageAccessToken = rootNode.get("access_token").asText();

        FacebookClient facebookClient = new DefaultFacebookClient(Facebook.pageAccessToken, Version.VERSION_8_0);

        Page page = facebookClient.fetchObject("me", Page.class, Parameter.with("fields","link,id"));

        Facebook.pageLink = page.getLink();

        Facebook.idPage = page.getId();

        Facebook.credentials = true;

        if (this.informationLoaderService.hasDataOriginInstance(Facebook.pageLink)) {

            this.informationLoaderService.saveInformation(socialType);

            ResponseDto responseDTO = new ResponseDto();

            responseDTO.setStatus(HttpStatus.OK.value());

            responseDTO.setMessage("Credentials have been sent");

            responseDTO.setData(true);

            return responseDTO;

        }else{

            ResponseDto responseDTO = new ResponseDto();

            responseDTO.setStatus(HttpStatus.BAD_REQUEST.value());

            responseDTO.setMessage("Error. Credentials already have been received");

            responseDTO.setData(false);

            return responseDTO;

        }

    }

}

