package com.tsoftlatam.rad.scb.SocialMicroservice.repository;

import com.tsoftlatam.rad.scb.SocialMicroservice.model.DataOriginInstanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface DataOriginInstanceRepository extends JpaRepository<DataOriginInstanceEntity,Integer> {

    @Query("SELECT d FROM dataorigininstance d WHERE d.url = :url")
    public Iterable<DataOriginInstanceEntity> findDataOriginInstanceByUrl(@Param("url") String url);

}
