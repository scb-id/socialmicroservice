package com.tsoftlatam.rad.scb.SocialMicroservice.repository;

import com.tsoftlatam.rad.scb.SocialMicroservice.model.SocialMediaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SocialMediaRepository extends JpaRepository<SocialMediaEntity,Integer> {



}
