package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook;

public class FacebookCredentialDto {

    private String shortLivePageAccessToken;

    private String idApp;

    private String appSecret;

    public String getShortLivePageAccessToken() {

        return shortLivePageAccessToken;

    }

    public void setShortLivePageAccessToken(String shortLivePageAccessToken) {

        this.shortLivePageAccessToken = shortLivePageAccessToken;

    }

    public String getIdApp() {

        return idApp;

    }

    public void setIdApp(String idApp) {

        this.idApp = idApp;

    }

    public String getAppSecret() {

        return appSecret;

    }

    public void setAppSecret(String appSecret) {

        this.appSecret = appSecret;

    }

}
