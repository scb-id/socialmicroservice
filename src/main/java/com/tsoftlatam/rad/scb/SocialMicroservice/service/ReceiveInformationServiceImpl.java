package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.factory.InformationReceiverFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

@Service
public class ReceiveInformationServiceImpl implements ReceiveInformationService {

    @Override
    public ResponseDto receiveInformation(HttpServletRequest request) throws IOException {

        BufferedReader bufferedReader = request.getReader();

        String json = bufferedReader.readLine();

        System.out.println("Lo que envio el microservicio de bot "+json);

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode pRootNode = objectMapper.readTree(json);

        String type = pRootNode.get("messageData").get("type").asText();

        InformationReceiverService informationReceiverServiceSocialMedia = InformationReceiverFactory.build(type);

        return informationReceiverServiceSocialMedia.receiveInformation(json, pRootNode);
            
    }

}
