package com.tsoftlatam.rad.scb.SocialMicroservice.model;

import javax.persistence.*;

@Entity(name = "attributedataorigininstance")
@SequenceGenerator(name = "attribute_dataorigininstance_id_seq",initialValue = 1, sequenceName = "attribute_dataorigininstance_id_seq", allocationSize = 1)
public class AttributeDataOriginInstanceEntity {

    @Id
    @Column(name = "idattributedataorigininstance")
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "attribute_dataorigininstance_id_seq")
    private Integer idAttributeDataOriginInstance;

    @JoinColumn(name = "idattribute")
    @ManyToOne
    private AttributeEntity attributeEntity;

    @JoinColumn(name = "iddataorigininstance")
    @ManyToOne
    private DataOriginInstanceEntity dataOriginInstanceEntity;

    @Column(name = "value")
    private String value;

    public AttributeDataOriginInstanceEntity() {



    }

    public Integer getIdAttributeDataOriginInstance() {

        return idAttributeDataOriginInstance;

    }

    public void setIdAttributeDataOriginInstance(Integer idAttributeDataOriginInstance) {

        this.idAttributeDataOriginInstance = idAttributeDataOriginInstance;

    }

    public AttributeEntity getAttributeEntity() {

        return attributeEntity;

    }

    public void setAttributeEntity(AttributeEntity attributeEntity) {

        this.attributeEntity = attributeEntity;

    }

    public DataOriginInstanceEntity getDataOriginInstanceEntity() {

        return dataOriginInstanceEntity;

    }

    public void setDataOriginInstanceEntity(DataOriginInstanceEntity dataOriginInstanceEntity) {

        this.dataOriginInstanceEntity = dataOriginInstanceEntity;

    }

    public String getValue() {

        return value;

    }

    public void setValue(String value) {

        this.value = value;

    }

}
