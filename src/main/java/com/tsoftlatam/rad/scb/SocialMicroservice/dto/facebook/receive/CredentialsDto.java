package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive;

public class CredentialsDto {

    private String socialType;

    private String shortLivePageAccessToken;

    private String idApp;

    private String appSecret;

    public String getSocialType() {

        return socialType;

    }

    public void setSocialType(String socialType) {

        this.socialType = socialType;

    }

    public String getShortLivePageAccessToken() {

        return shortLivePageAccessToken;

    }

    public void setShortLivePageAccessToken(String shortLivePageAccessToken) {

        this.shortLivePageAccessToken = shortLivePageAccessToken;

    }

    public String getIdApp() {

        return idApp;

    }

    public void setIdApp(String idApp) {

        this.idApp = idApp;

    }

    public String getAppSecret() {

        return appSecret;

    }

    public void setAppSecret(String appSecret) {

        this.appSecret = appSecret;

    }

    @Override
    public String toString()
    {
        return "ClassPojo [idApp = "+idApp+", shortLivePageAccessToken = "+shortLivePageAccessToken+", appSecret = "+appSecret+", socialType = "+socialType+"]";
    }

}
