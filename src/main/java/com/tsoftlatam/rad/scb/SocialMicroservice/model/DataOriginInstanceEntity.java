package com.tsoftlatam.rad.scb.SocialMicroservice.model;

import javax.persistence.*;

@Entity(name = "dataorigininstance")
@SequenceGenerator(name = "dataorigininstance_id_seq",initialValue = 1, sequenceName = "dataorigininstance_id_seq", allocationSize = 1)
public class DataOriginInstanceEntity {

    @Id
    @Column(name = "iddataorigininstance")
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "dataorigininstance_id_seq")
    private Integer idDataOriginInstance;

    @Column(name = "url")
    private String url;

    public DataOriginInstanceEntity() {



    }

    public Integer getIdDataOriginInstance() {

        return idDataOriginInstance;

    }

    public void setIdDataOriginInstance(Integer idDataOriginInstance) {

        this.idDataOriginInstance = idDataOriginInstance;

    }

    public String getUrl() {

        return url;

    }

    public void setUrl(String url) {

        this.url = url;

    }

}
