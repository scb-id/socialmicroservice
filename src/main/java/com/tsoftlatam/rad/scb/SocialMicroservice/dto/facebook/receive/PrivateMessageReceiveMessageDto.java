package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive;

public class PrivateMessageReceiveMessageDto {

    private String mid;

    private String text;

    public String getMid ()
    {
        return mid;
    }

    public void setMid (String mid)
    {
        this.mid = mid;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [mid = "+mid+", text = "+text+"]";
    }

}
