package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;

import java.io.IOException;

public interface InformationReceiverService {

    public ResponseDto receiveInformation(String json, JsonNode pRootNode) throws IOException;

}
