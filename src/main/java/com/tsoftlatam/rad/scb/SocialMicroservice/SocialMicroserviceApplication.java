package com.tsoftlatam.rad.scb.SocialMicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialMicroserviceApplication {

	public static void main(String[] args){
		
		SpringApplication.run(SocialMicroserviceApplication.class, args);
		
	}

}