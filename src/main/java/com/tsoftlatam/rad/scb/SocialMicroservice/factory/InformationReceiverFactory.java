package com.tsoftlatam.rad.scb.SocialMicroservice.factory;

import com.tsoftlatam.rad.scb.SocialMicroservice.service.InformationReceiverService;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.facebook.FacebookInformationReceiverServiceImpl;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.twitter.TwitterInformationReceiverServiceImpl;

public class InformationReceiverFactory {

    public static InformationReceiverService build(String type) {

        if (type.equalsIgnoreCase("FACEBOOK")) {

            FacebookInformationReceiverServiceImpl facebookInformationReceiver = new FacebookInformationReceiverServiceImpl();

            return facebookInformationReceiver; // Se puede retornar una clase pese a que es diferente a una interfaz
            // ya que justamente la clase implementa a dicha interfaz.

        }else{

            TwitterInformationReceiverServiceImpl twitterInformationReceiver = new TwitterInformationReceiverServiceImpl();

            return twitterInformationReceiver; // Se puede retornar una clase pese a que es diferente a una interfaz
            // ya que justamente la clase implementa a dicha interfaz.

        }

    }

}
