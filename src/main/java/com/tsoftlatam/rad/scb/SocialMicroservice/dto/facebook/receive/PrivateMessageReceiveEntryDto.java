package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive;

public class PrivateMessageReceiveEntryDto {

    private String id;

    private String time;

    private PrivateMessageReceiveMessagingDto[] messaging;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public PrivateMessageReceiveMessagingDto[] getMessaging ()
    {
        return messaging;
    }

    public void setMessaging (PrivateMessageReceiveMessagingDto[] messaging)
    {
        this.messaging = messaging;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", time = "+time+", messaging = "+messaging+"]";
    }

}
