package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive;

public class PrivateMessageReceiveMessagingDto {

    private PrivateMessageReceiveSenderDto sender;

    private PrivateMessageReceiveRecipientDto recipient;

    private PrivateMessageReceiveMessageDto message;

    private PrivateMessageReceivePostbackDto postback;

    private String timestamp;

    public PrivateMessageReceiveSenderDto getSender ()
    {
        return sender;
    }

    public void setSender (PrivateMessageReceiveSenderDto sender)
    {
        this.sender = sender;
    }

    public PrivateMessageReceiveRecipientDto getRecipient ()
    {
        return recipient;
    }

    public void setRecipient (PrivateMessageReceiveRecipientDto recipient)
    {
        this.recipient = recipient;
    }

    public PrivateMessageReceiveMessageDto getMessage ()
    {
        return message;
    }

    public void setMessage (PrivateMessageReceiveMessageDto message)
    {
        this.message = message;
    }

    public String getTimestamp ()
    {
        return timestamp;
    }

    public void setTimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    public PrivateMessageReceivePostbackDto getPostback() {

        return postback;

    }

    public void setPostback(PrivateMessageReceivePostbackDto postback) {

        this.postback = postback;

    }

    @Override
    public String toString()
    {
        return "ClassPojo [sender = "+sender+", recipient = "+recipient+", message = "+message+", timestamp = "+timestamp+"]";
    }

}
