package com.tsoftlatam.rad.scb.SocialMicroservice.service.facebook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.send.*;
import com.tsoftlatam.rad.scb.SocialMicroservice.configuration.Facebook;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive.PrivateMessageReceiveRootDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response.PrivateMessageResponseButtonsDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response.PrivateMessageResponseRootDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.InformationReceiverService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class FacebookInformationReceiverServiceImpl implements InformationReceiverService {

    public boolean isPost;

    public ResponseDto receiveInformation (String json, JsonNode pRootNode) throws JsonProcessingException {

        if (Facebook.credentials) {

            isPost = pRootNode.has("post_id");

            if (isPost) {

                this.sendInformationPostToUser(json, pRootNode);

            }else{

                this.sendInformationMessageToUser(json, pRootNode);

            }

            ResponseDto responseDTO = new ResponseDto();

            responseDTO.setStatus(HttpStatus.OK.value());

            responseDTO.setMessage("Information has been received");

            responseDTO.setData(true);

            return responseDTO;

        }else{

            ResponseDto responseDTO = new ResponseDto();

            responseDTO.setStatus(HttpStatus.BAD_REQUEST.value());

            responseDTO.setMessage("Credentials must be sent first");

            responseDTO.setData(false);

            return responseDTO;

        }

    }

    public void sendInformationPostToUser (String json, JsonNode pRootNode) {

        // Se responde al usuario con un mensaje privado a la publicacion que realizo.

    }

    public void sendInformationMessageToUser (String json, JsonNode pRootNode) throws JsonProcessingException {

        // Implementar metodo en la misma clase para simplificar.

        if (pRootNode.get("messageData").get("message").has("attachment")) {

            ObjectMapper mapper = new ObjectMapper();

            PrivateMessageResponseRootDto privateMessageResponseRootDto =
                    mapper.readValue(json, PrivateMessageResponseRootDto.class);

            String recipientId = privateMessageResponseRootDto.getMessageData().getRecipient().getId();

            String title = privateMessageResponseRootDto.getMessageData().getMessage().getAttachment()
                    .getPayload().getElements()[0].getTitle();

            String imageUrl = privateMessageResponseRootDto.getMessageData().getMessage()
                    .getAttachment().getPayload().getElements()[0].getImage_url();

            String subTitle = privateMessageResponseRootDto.getMessageData().getMessage().
                    getAttachment().getPayload().getElements()[0].getSubtitle();

            PrivateMessageResponseButtonsDto buttons [];

            buttons = privateMessageResponseRootDto.getMessageData().getMessage().
                    getAttachment().getPayload().getElements()[0].getButtons();

            FacebookClient facebookClient = new DefaultFacebookClient(Facebook.pageAccessToken,
                    Version.VERSION_8_0);

            IdMessageRecipient recipient = new IdMessageRecipient(recipientId);

            GenericTemplatePayload payload = new GenericTemplatePayload();

            Bubble firstBubble = new Bubble(title);

            firstBubble.setImageUrl(imageUrl);

            firstBubble.setSubtitle(subTitle);

            PostbackButton postbackButtons [] = new PostbackButton[buttons.length];

            for (int i = 0; i < postbackButtons.length; i++) {

                postbackButtons[i] = new PostbackButton(buttons[i].getTitle(),
                        buttons[i].getPayload());

                firstBubble.addButton(postbackButtons[i]);

            }

            payload.addBubble(firstBubble);

            TemplateAttachment templateAttachment = new TemplateAttachment(payload);

            Message imageMessage = new Message(templateAttachment);

            SendResponse privateMessage = facebookClient.publish( Facebook.idPage + "/messages", SendResponse.class,
                    Parameter.with("recipient", recipient), Parameter.with("message", imageMessage));

        }else{

            // Implementar metodo en la misma clase para simplificar.

            ObjectMapper mapper = new ObjectMapper();

            PrivateMessageResponseRootDto privateMessageResponseRootDto = mapper.readValue(json, PrivateMessageResponseRootDto.class);

            String recipientId = privateMessageResponseRootDto.getMessageData().getRecipient().getId();

            String text = privateMessageResponseRootDto.getMessageData().getMessage().getText();

            FacebookClient facebookClient =
                    new DefaultFacebookClient(Facebook.pageAccessToken, Version.VERSION_8_0);

            IdMessageRecipient recipient =
                    new IdMessageRecipient(recipientId);

            Message message = new Message(text);

            System.out.println(recipientId+text
            +Facebook.idPage);

            SendResponse privateMessage = facebookClient.publish( Facebook.idPage + "/messages", SendResponse.class,
                    Parameter.with("recipient", recipient), Parameter.with("message", message));

        }

    }

}
