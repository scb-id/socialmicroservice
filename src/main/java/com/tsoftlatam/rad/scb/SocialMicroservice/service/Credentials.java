package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;

public interface Credentials {

    public ResponseDto createCredentials(String json, JsonNode rootNodeCredentials) throws JsonProcessingException;

}
