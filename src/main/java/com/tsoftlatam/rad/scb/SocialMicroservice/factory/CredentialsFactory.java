package com.tsoftlatam.rad.scb.SocialMicroservice.factory;

import com.tsoftlatam.rad.scb.SocialMicroservice.service.Credentials;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.facebook.FacebookCredentialsImpl;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.twitter.TwitterCredentialsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Component
public class CredentialsFactory {

    @Autowired
    private FacebookCredentialsImpl facebookCredentials;

    @Autowired
    private TwitterCredentialsImpl twitterCredentials;

    public Credentials build(String type) {

        if (type.equalsIgnoreCase("FACEBOOK")) {

            return this.facebookCredentials;

        }else{

            return this.twitterCredentials;

        }

    }

}
