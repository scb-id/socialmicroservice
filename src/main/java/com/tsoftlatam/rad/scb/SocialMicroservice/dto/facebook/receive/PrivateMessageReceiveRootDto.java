package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive;

public class PrivateMessageReceiveRootDto {

    private PrivateMessageReceiveEntryDto[] entry;

    private String object;

    public PrivateMessageReceiveEntryDto[] getEntry ()
    {
        return entry;
    }

    public void setEntry (PrivateMessageReceiveEntryDto[] entry)
    {
        this.entry = entry;
    }

    public String getObject ()
    {
        return object;
    }

    public void setObject (String object)
    {
        this.object = object;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [entry = "+entry+", object = "+object+"]";
    }

}


