package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response;

public class PrivateMessageResponsePayloadDto {

    private PrivateMessageResponseElementsDto[] elements;

    private String template_type;

    public PrivateMessageResponseElementsDto[] getElements ()
    {
        return elements;
    }

    public void setElements (PrivateMessageResponseElementsDto[] elements)
    {
        this.elements = elements;
    }

    public String getTemplate_type ()
    {
        return template_type;
    }

    public void setTemplate_type (String template_type)
    {
        this.template_type = template_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [elements = "+elements+", template_type = "+template_type+"]";
    }

}
