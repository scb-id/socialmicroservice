package com.tsoftlatam.rad.scb.SocialMicroservice.controller.facebook;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.FacebookWebHookConfigurationDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.WebHookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/SocialMicroservice")
public class FacebookWebHookController {

    @Autowired
    private WebHookService webHookService;

    @RequestMapping(value = "/getrequest", method = RequestMethod.GET)
    public String getVerificationRequest(@RequestParam(value = "hub.mode") String mode, @RequestParam(value = "hub.challenge") String challenge, @RequestParam(value = "hub.verify_token") String verify_token) {

        if(mode.equalsIgnoreCase("subscribe") && verify_token.equalsIgnoreCase("Token")){

            return challenge;

        }else {

            return "Rejected Request";

        }

    }

    @RequestMapping(value = "/getrequest", method = RequestMethod.POST)
    public ResponseEntity<Object> getEventNotification(HttpServletRequest request) throws IOException, UnirestException {

        return this.webHookService.getEventNotification(request);

    }

    @RequestMapping(value = "/webhookconfiguration", method = RequestMethod.POST)
    public ResponseEntity<ResponseDto> webhookPageConfiguration (@RequestBody FacebookWebHookConfigurationDto facebookWebHookConfigurationDto) {

        ResponseDto responseDTO = new ResponseDto();

        try {
 
            responseDTO = this.webHookService.webhookConfiguration(facebookWebHookConfigurationDto);

            int code = responseDTO.getStatus();

            return new ResponseEntity<>(responseDTO, HttpStatus.valueOf(code));

        }catch (Exception ex) {

            responseDTO.setStatus(HttpStatus.BAD_REQUEST.value());

            responseDTO.setMessage("Bad Request");

            responseDTO.setData(null);

            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);

        }

    }

}
