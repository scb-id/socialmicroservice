package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.factory.CredentialsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;

import java.io.BufferedReader;
import java.io.IOException;

@Service
public class CredentialsServiceImpl implements CredentialsService {

    @Autowired
    private CredentialsFactory credentialsFactory;

    @Override
    public ResponseDto createCredentials(HttpServletRequest request) throws IOException {

        BufferedReader bufferedReader = request.getReader();

        String json = bufferedReader.readLine();

        System.out.println(json);

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode rootNodeCredentials = objectMapper.readTree(json);

        String type = rootNodeCredentials.get("socialType").asText();

        Credentials credentials = this.credentialsFactory.build(type);

        ResponseDto responseDto = credentials.createCredentials(json, rootNodeCredentials);

        return responseDto;

    }

}
