package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response;

public class PrivateMessageResponseMessageDto {

    private String text;

    private PrivateMessageResponseAttachmentDto attachment;

    public String getText() {

        return text;

    }

    public void setText(String text) {

        this.text = text;

    }

    public PrivateMessageResponseAttachmentDto getAttachment ()
    {
        return attachment;
    }

    public void setAttachment (PrivateMessageResponseAttachmentDto attachment)
    {
        this.attachment = attachment;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [attachment = "+attachment+"]";
    }

}
