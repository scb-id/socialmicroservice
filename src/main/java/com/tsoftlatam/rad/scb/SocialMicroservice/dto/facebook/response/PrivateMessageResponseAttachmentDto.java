package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response;

public class PrivateMessageResponseAttachmentDto {

    private PrivateMessageResponsePayloadDto payload;

    private String type;

    public PrivateMessageResponsePayloadDto getPayload ()
    {
        return payload;
    }

    public void setPayload (PrivateMessageResponsePayloadDto payload)
    {
        this.payload = payload;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [payload = "+payload+", type = "+type+"]";
    }

}
