package com.tsoftlatam.rad.scb.SocialMicroservice.controller;

import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.CredentialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/SocialMicroservice")
public class CredentialsController {

    @Autowired
    private CredentialsService credentialsService;

    @RequestMapping(value = "/credentials", method = RequestMethod.POST)
    public ResponseEntity<ResponseDto> credentials (HttpServletRequest request)  {

        ResponseDto responseDTO = new ResponseDto();

        try {

            responseDTO = this.credentialsService.createCredentials(request);

            return new ResponseEntity<>(responseDTO, HttpStatus.OK);

        }catch (Exception ex) {

            responseDTO.setStatus(HttpStatus.BAD_REQUEST.value());

            responseDTO.setMessage("Bad Request");

            responseDTO.setData(null);

            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);

        }

    }

}
