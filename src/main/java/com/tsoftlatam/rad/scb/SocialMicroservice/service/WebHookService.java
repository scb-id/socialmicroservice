package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.FacebookWebHookConfigurationDto;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface WebHookService {

    public ResponseEntity<Object> getEventNotification (HttpServletRequest request) throws IOException, UnirestException;

    public ResponseDto webhookConfiguration (FacebookWebHookConfigurationDto facebookWebHookConfigurationDto);

}
