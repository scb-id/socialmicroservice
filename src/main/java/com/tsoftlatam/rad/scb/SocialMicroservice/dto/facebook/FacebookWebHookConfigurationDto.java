package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook;

public class FacebookWebHookConfigurationDto {

    private String appAccessToken;

    private String object;

    private String fields;

    private String subscribedFields;

    public String getAppAccessToken() {

        return appAccessToken;

    }

    public void setAppAccessToken(String appAccessToken) {

        this.appAccessToken = appAccessToken;

    }

    public String getObject() {

        return object;

    }

    public void setObject(String object) {

        this.object = object;

    }

    public String getFields() {

        return fields;

    }

    public void setFields(String fields) {

        this.fields = fields;

    }

    public String getSubscribedFields() {

        return subscribedFields;

    }

    public void setSubscribedFields(String subscribedFields) {

        this.subscribedFields = subscribedFields;

    }

}
