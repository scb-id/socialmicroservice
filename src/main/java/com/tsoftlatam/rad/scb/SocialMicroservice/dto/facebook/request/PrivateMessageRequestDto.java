package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.request;

public class PrivateMessageRequestDto {

    private String idSender;

    private String payload;

    private String text;

    private String hashIdSender;

    private String inputMethod;

    private String fullName;

    private String type;

    private String url;

    public String getIdSender() {

        return idSender;

    }

    public void setIdSender(String idSender) {

        this.idSender = idSender;

    }

    public String getPayload() {

        return payload;

    }

    public void setPayload(String payload) {

        this.payload = payload;

    }

    public String getText() {

        return text;

    }

    public void setText(String text) {

        this.text = text;

    }

    public String getHashIdSender() {

        return hashIdSender;

    }

    public void setHashIdSender(String hashIdSender) {

        this.hashIdSender = hashIdSender;

    }

    public String getInputMethod() {

        return inputMethod;

    }

    public void setInputMethod(String inputMethod) {

        this.inputMethod = inputMethod;

    }

    public String getFullName() {

        return fullName;

    }

    public void setFullName(String fullName) {

        this.fullName = fullName;

    }

    public String getType() {

        return type;

    }

    public void setType(String type) {

        this.type = type;

    }

    public String getUrl() {

        return url;

    }

    public void setUrl(String url) {

        this.url = url;

    }

}
