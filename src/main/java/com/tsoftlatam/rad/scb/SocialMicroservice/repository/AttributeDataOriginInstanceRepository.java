package com.tsoftlatam.rad.scb.SocialMicroservice.repository;

import com.tsoftlatam.rad.scb.SocialMicroservice.model.AttributeDataOriginInstanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttributeDataOriginInstanceRepository extends JpaRepository<AttributeDataOriginInstanceEntity, Integer> {



}
