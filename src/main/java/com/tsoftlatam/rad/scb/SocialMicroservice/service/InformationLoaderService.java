package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.tsoftlatam.rad.scb.SocialMicroservice.configuration.Facebook;
import com.tsoftlatam.rad.scb.SocialMicroservice.model.AttributeDataOriginInstanceEntity;
import com.tsoftlatam.rad.scb.SocialMicroservice.model.AttributeEntity;
import com.tsoftlatam.rad.scb.SocialMicroservice.model.DataOriginInstanceEntity;
import com.tsoftlatam.rad.scb.SocialMicroservice.model.SocialMediaEntity;
import com.tsoftlatam.rad.scb.SocialMicroservice.repository.AttributeDataOriginInstanceRepository;
import com.tsoftlatam.rad.scb.SocialMicroservice.repository.AttributeRepository;
import com.tsoftlatam.rad.scb.SocialMicroservice.repository.DataOriginInstanceRepository;
import com.tsoftlatam.rad.scb.SocialMicroservice.repository.SocialMediaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InformationLoaderService implements ApplicationRunner {

    @Autowired
    private SocialMediaRepository socialMediaRepository;

    @Autowired
    private AttributeRepository attributeRepository;

    @Autowired
    private DataOriginInstanceRepository dataOriginInstanceRepository;

    @Autowired
    private AttributeDataOriginInstanceRepository attributeDataOriginInstanceRepository;

    @Override
    public void run(ApplicationArguments args) {



    }

    public boolean hasDataOriginInstance (String url) {

        Iterable<DataOriginInstanceEntity> dataOriginInstanceEntityIterable =
                this.dataOriginInstanceRepository.findDataOriginInstanceByUrl(url);

        List<DataOriginInstanceEntity> dataOriginInstanceEntityList =
                (List<DataOriginInstanceEntity>) dataOriginInstanceEntityIterable;

        return dataOriginInstanceEntityList.isEmpty();

    }

    public void saveInformation (String pSocialType) {

        // Data Source Instance Repository

        DataOriginInstanceEntity dataOriginInstanceEntity =
                new DataOriginInstanceEntity();

        dataOriginInstanceEntity.setUrl(Facebook.pageLink);

        this.dataOriginInstanceRepository.save(dataOriginInstanceEntity);

        // Social Media Entity

        SocialMediaEntity socialMediaEntity = new SocialMediaEntity();

        socialMediaEntity.setSocialType(pSocialType);

        this.socialMediaRepository.save(socialMediaEntity);

        // Attribute Entity

        String [] attributes = {"Permanent Page Access Token", "Id App", "App Secret"};

        AttributeEntity[] attributeEntity = new AttributeEntity[3];

        for (int i = 0; i < 3; i++) {

            AttributeEntity elementAttributeEntity = new AttributeEntity();

            elementAttributeEntity.setDescription(attributes[i]);

            elementAttributeEntity.setIdSocialMedia(socialMediaEntity);

            attributeEntity[i] = elementAttributeEntity;

            this.attributeRepository.save(attributeEntity[i]);

        }

        // Attribute Data Source Instance Repository

        String values [] = {Facebook.pageAccessToken, Facebook.idApp, Facebook.appSecret};

        for (int i = 0; i < values.length; i++) {

            AttributeDataOriginInstanceEntity attributeDataOriginInstanceEntity =
                    new AttributeDataOriginInstanceEntity();

            attributeDataOriginInstanceEntity.setValue(values[i]);

            attributeDataOriginInstanceEntity.setDataOriginInstanceEntity(dataOriginInstanceEntity);

            attributeDataOriginInstanceEntity.
                    setAttributeEntity(attributeEntity[i]);

            this.attributeDataOriginInstanceRepository.save(attributeDataOriginInstanceEntity);

        }

    }

    public boolean hasInformation () {

        // Recuperar los origenes de datos.
        
        // Para cada origen de datos, subscribirse a los eventos de los webhooks

        Iterable<DataOriginInstanceEntity> dataOriginInstanceEntityIterable =
                this.dataOriginInstanceRepository.findAll();

        List<DataOriginInstanceEntity> dataOriginInstanceEntityList =
                (List<DataOriginInstanceEntity>) dataOriginInstanceEntityIterable;

        return dataOriginInstanceEntityList.isEmpty();

    }

    public void getInformation () {

        

    }

}
