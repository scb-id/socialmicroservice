package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface ReceiveInformationService {

    public ResponseDto receiveInformation (HttpServletRequest request) throws IOException;

}
