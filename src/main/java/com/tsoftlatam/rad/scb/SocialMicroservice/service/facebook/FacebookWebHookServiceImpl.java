package com.tsoftlatam.rad.scb.SocialMicroservice.service.facebook;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.Application;
import com.restfb.types.Page;
import com.restfb.types.User;
import com.tsoftlatam.rad.scb.SocialMicroservice.configuration.Communicator;
import com.tsoftlatam.rad.scb.SocialMicroservice.configuration.Facebook;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive.PrivateMessageReceiveRootDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.request.PrivateMessageRequestDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.FacebookWebHookConfigurationDto;
import com.tsoftlatam.rad.scb.SocialMicroservice.service.WebHookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;

@Service
public class FacebookWebHookServiceImpl implements WebHookService {

    public boolean isPost = false;

    public ResponseEntity<Object> getEventNotification (HttpServletRequest request) throws IOException, UnirestException {

        if (Facebook.credentials) {

            ResponseEntity<Object> response = new ResponseEntity<>(HttpStatus.OK);

            BufferedReader bufferedReader = request.getReader();

            String json = bufferedReader.readLine();

            System.out.println("Lo que envio el usuario "+json);

            ObjectMapper objectMapper = new ObjectMapper();

            JsonNode rootNode = objectMapper.readTree(json);

            this.isPost = rootNode.get("entry").get(0).has("changes");

            if (this.isPost) {

                this.sendInformationPost(json, rootNode);

            }else{

                this.sendInformationMessage(json, rootNode);

            }

            return response;

        }else{

            ResponseEntity<Object> response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);

            return response;

        }

    }

    public void sendInformationPost (String json, JsonNode pRootNode) throws UnirestException {

        // Deserializacion

        // Serializacion

        String iduser = pRootNode.get("entry").get(0).get("changes").get(0).
                get("value").get("from").get("id").asText();

        String text = pRootNode.get("entry").get(0).get("changes").get(0).get("value").
                get("message").asText();

        try{

            Unirest.setTimeouts(0, 0);

            String variable = "{\"iduser\":\""+iduser+"\",\"text\":\""+text+"\"}";

            //HttpResponse<com.mashape.unirest.http.JsonNode> request = Unirest.post(Communicator.SEND_INFORMATION_POST_URl).header("Content-Type", "application/json").body(variable).asJson();

        }catch (Exception ex) {

            System.out.println("La excepcion es "+ex);

        }

    }

    public void sendInformationMessage (String json, JsonNode pRootNode) throws JsonProcessingException {

        if (pRootNode.get("entry").get(0).get("messaging").get(0).has("postback")) {

            // Deserializacion

            ObjectMapper mapper = new ObjectMapper();

            PrivateMessageReceiveRootDto privateMessageReceiveRootDto = mapper.readValue(json, PrivateMessageReceiveRootDto.class);

            // Serializacion

            PrivateMessageRequestDto privateMessageRequestDto = new PrivateMessageRequestDto();

            privateMessageRequestDto.setIdSender(privateMessageReceiveRootDto.getEntry()[0].
                    getMessaging()[0].getSender().getId());

            privateMessageRequestDto.setPayload(privateMessageReceiveRootDto.getEntry()[0].getMessaging()[0].getPostback().getPayload());

            privateMessageRequestDto.setHashIdSender(Integer.toString(privateMessageRequestDto.getIdSender().hashCode()));

            privateMessageRequestDto.setInputMethod("PrivateMessage");

            FacebookClient facebookClient = new DefaultFacebookClient(Facebook.pageAccessToken, Version.VERSION_8_0);

            User user = facebookClient.fetchObject(privateMessageRequestDto.getIdSender(), User.class, Parameter.with("fields","first_name, last_name"));

            privateMessageRequestDto.setFullName(user.getFirstName() + " " + user.getLastName());

            privateMessageRequestDto.setType(Facebook.TYPE);

            privateMessageRequestDto.setUrl(Facebook.pageLink);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonStringPrivateMessage = objectMapper.writeValueAsString(privateMessageRequestDto);

            try{

                Unirest.setTimeouts(0, 0);

                HttpResponse<com.mashape.unirest.http.JsonNode> request = Unirest.post(Communicator.SEND_INFORMATION_MESSAGE_URL).
                        header("Content-Type", "application/json").body(jsonStringPrivateMessage)
                        .asJson();

            }catch (Exception ex) {

                System.out.println("La excepcion es: "+ex.getMessage());

            }

        }else{

            // Deserializacion

            ObjectMapper mapper = new ObjectMapper();

            PrivateMessageReceiveRootDto privateMessageReceiveRootDto = mapper.readValue(json, PrivateMessageReceiveRootDto.class);

            // Serializacion

            PrivateMessageRequestDto privateMessageRequestDto = new PrivateMessageRequestDto();

            privateMessageRequestDto.setIdSender(privateMessageReceiveRootDto.getEntry()[0].getMessaging()[0].getSender().getId());

            privateMessageRequestDto.setText(privateMessageReceiveRootDto.getEntry()[0].getMessaging()[0].getMessage().getText());

            privateMessageRequestDto.setHashIdSender(Integer.toString(privateMessageRequestDto.getIdSender().hashCode()));

            privateMessageRequestDto.setInputMethod("PrivateMessage");

            FacebookClient facebookClient = new DefaultFacebookClient(Facebook.pageAccessToken, Version.VERSION_8_0);

            User user = facebookClient.fetchObject(privateMessageRequestDto.getIdSender(), User.class, Parameter.with("fields","first_name, last_name"));

            privateMessageRequestDto.setFullName(user.getFirstName() + " " + user.getLastName());

            privateMessageRequestDto.setType(Facebook.TYPE);

            privateMessageRequestDto.setUrl(Facebook.pageLink);

            ObjectMapper objectMapper = new ObjectMapper();

            String jsonStringPrivateMessage = objectMapper.writeValueAsString(privateMessageRequestDto);

            try {

                Unirest.setTimeouts(0, 0);

                HttpResponse<com.mashape.unirest.http.JsonNode> request = Unirest.post(Communicator.SEND_INFORMATION_MESSAGE_URL).
                        header("Content-Type", "application/json").body(jsonStringPrivateMessage)
                        .asJson();

            }catch (Exception ex) {

                System.out.println("La excepcion fue "+ex.getMessage());

            }

        }

    }

    public ResponseDto webhookConfiguration (FacebookWebHookConfigurationDto facebookWebHookConfigurationDto) {

        if (Facebook.credentials) {

            FacebookClient facebookClient = new DefaultFacebookClient(facebookWebHookConfigurationDto.getAppAccessToken(),
                    Version.VERSION_8_0);

            Application app = facebookClient.publish("app/subscriptions", Application.class,
                    Parameter.with("object", facebookWebHookConfigurationDto.getObject()),
                    Parameter.with("fields", facebookWebHookConfigurationDto.getFields()),
                    Parameter.with("callback_url", Communicator.CALLBACK_URL),
                    Parameter.with("verify_token","Token"));

            facebookClient = null;

            FacebookClient otherFacebookClient = new DefaultFacebookClient(Facebook.pageAccessToken, Version.VERSION_8_0);

            Page page = otherFacebookClient.publish(Facebook.idPage + "/subscribed_apps", Page.class, Parameter.with("subscribed_fields", facebookWebHookConfigurationDto.getSubscribedFields()));

            ResponseDto responseDTO = new ResponseDto();

            responseDTO.setStatus(HttpStatus.OK.value());

            responseDTO.setMessage("Webhook Page has been configured");

            responseDTO.setData(true);

            return responseDTO;

        }else{

            ResponseDto responseDTO = new ResponseDto();

            responseDTO.setStatus(HttpStatus.BAD_REQUEST.value());

            responseDTO.setMessage("Credentials must be sent first");

            responseDTO.setData(false);

            return responseDTO;

        }

    }

}
