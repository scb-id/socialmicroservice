package com.tsoftlatam.rad.scb.SocialMicroservice.configuration;

public class Facebook {

    public static final String TYPE = "FACEBOOK";

    public static String idPage;

    public static String pageAccessToken;

    public static String pageLink;

    public static String idApp;

    public static String appSecret;

    public static boolean credentials = false;

}
