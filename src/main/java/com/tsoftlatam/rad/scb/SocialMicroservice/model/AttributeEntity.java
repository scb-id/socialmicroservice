package com.tsoftlatam.rad.scb.SocialMicroservice.model;

import javax.persistence.*;

@Entity(name = "attribute")
@SequenceGenerator(name = "attribute_id_seq",initialValue = 1, sequenceName = "attribute_id_seq", allocationSize = 1)
public class AttributeEntity {

    @Id
    @Column(name = "idattribute")
    @GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "attribute_id_seq")
    private Integer idAttribute;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "idsocialmedia")
    private SocialMediaEntity idSocialMedia;

    public AttributeEntity() {



    }

    public Integer getIdAttribute() {

        return idAttribute;

    }

    public void setIdAttribute(Integer idAttribute) {

        this.idAttribute = idAttribute;

    }

    public String getDescription() {

        return description;

    }

    public void setDescription(String description) {

        this.description = description;

    }

    public SocialMediaEntity getIdSocialMedia() {

        return idSocialMedia;

    }

    public void setIdSocialMedia(SocialMediaEntity idSocialMedia) {

        this.idSocialMedia = idSocialMedia;

    }

}
