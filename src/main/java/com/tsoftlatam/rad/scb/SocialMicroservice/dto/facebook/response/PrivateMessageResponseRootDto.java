package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response;

public class PrivateMessageResponseRootDto {

    private PrivateMessageResponseMessageDataDto messageData;

    public PrivateMessageResponseMessageDataDto getMessageData ()
    {
        return messageData;
    }

    public void setMessageData (PrivateMessageResponseMessageDataDto messageData)
    {
        this.messageData = messageData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [messageData = "+messageData+"]";
    }

}
