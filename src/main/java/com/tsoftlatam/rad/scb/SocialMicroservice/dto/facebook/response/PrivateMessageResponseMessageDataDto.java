package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.response;

public class PrivateMessageResponseMessageDataDto {

    private PrivateMessageResponseRecipientDto recipient;

    private PrivateMessageResponseMessageDto message;

    private String type;

    public PrivateMessageResponseRecipientDto getRecipient ()
    {
        return recipient;
    }

    public void setRecipient (PrivateMessageResponseRecipientDto recipient)
    {
        this.recipient = recipient;
    }

    public PrivateMessageResponseMessageDto getMessage ()
    {
        return message;
    }

    public void setMessage (PrivateMessageResponseMessageDto message)
    {
        this.message = message;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [recipient = "+recipient+", message = "+message+", type = "+type+"]";
    }

}
