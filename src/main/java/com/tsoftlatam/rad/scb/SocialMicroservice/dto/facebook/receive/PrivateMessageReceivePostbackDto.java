package com.tsoftlatam.rad.scb.SocialMicroservice.dto.facebook.receive;

public class PrivateMessageReceivePostbackDto {

    private String payload;

    private String title;

    public String getPayload ()
    {
        return payload;
    }

    public void setPayload (String payload)
    {
        this.payload = payload;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [payload = "+payload+", title = "+title+"]";
    }

}
