package com.tsoftlatam.rad.scb.SocialMicroservice.repository;

import com.tsoftlatam.rad.scb.SocialMicroservice.model.AttributeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttributeRepository extends JpaRepository<AttributeEntity,Integer> {



}
