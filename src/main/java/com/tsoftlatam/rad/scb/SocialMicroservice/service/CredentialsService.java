package com.tsoftlatam.rad.scb.SocialMicroservice.service;

import com.tsoftlatam.rad.scb.SocialMicroservice.dto.ResponseDto;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface CredentialsService {

    public ResponseDto createCredentials(HttpServletRequest request) throws IOException;

}
